import React from 'react';
import { useSelector } from 'react-redux';

const Overview = () => {
    const getInfoTransactions = useSelector((state) => state.transactions);
    const getTransactions = (elem, item) => getInfoTransactions.filter(transaction  => transaction[item] === elem);
    const mapTransactions = (elem, item) => getTransactions(elem, item).map((item) => item.value)
    const reduceTrans = (elem, item) => mapTransactions(elem, item).reduce((sum, curr) => sum + curr, 0);

    const cashFlow = reduceTrans('income', 'type') - reduceTrans('expense', 'type');
    return (
        <>
        {getInfoTransactions === 0 ? (
        <h2>Not found</h2>
        ) :
        <>
        <h2>Income</h2>
        <ul>
            <li>Income salary: {reduceTrans('salary','category')}</li>
            <li>Income bonus: {reduceTrans('bonus','category')}</li>
        </ul>
        <h2>Expenses</h2>
        <ul>
             <li>Expenses product: {reduceTrans('product', 'category')}</li>
            <li>Expenses taxi: {reduceTrans('taxi', 'category')}</li>
            <li>Expenses leisure: {reduceTrans('leisure', 'category')}</li>
            <li>Expenses club: {reduceTrans('club', 'category')}</li>
            <li>Expenses travel: {reduceTrans('travel', 'category')}</li> 
        </ul>
        <h2>General</h2>
        <ul>
        <li>Summary income transactions: {reduceTrans('income', 'type')}</li>
        <li>Summary expense transactions: {reduceTrans('expense', 'type')}</li>
        <li>Cash flow: {cashFlow}</li>
        {cashFlow < 0 ? <span style={{color:'red'}}>Ты транжира</span> :
        (
            <span style={{color:'green'}}>Еще осталось {cashFlow} монет </span>
        )}
    </ul>
    </>
        }
        </>
        
    )
}

export default Overview;