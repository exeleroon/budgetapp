import React, { useEffect } from "react";
import Header from './component/Header';
import Routes from './Routers/route';
import { getTransactions as fetchTransactionsApi } from './api/getTransactions'
import { useDispatch } from 'react-redux';
import { setTransactions } from './store/actions';

const App = () => {
  const dispatch = useDispatch();
    useEffect(() => {
      fetchTransactionsApi().then((transactions) => {
        dispatch(setTransactions(transactions.data))
      })
    }, [])
  return (
    <>
      <Header />
      <Routes />
    </>
  );
}

export default App;
