import { createStore } from 'redux';
import rootReducer from './rootReducer.js';

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__
  ? window.__REDUX_DEVTOOLS_EXTENSION__()
  : (f) => f;

  const store = createStore(
    rootReducer,
    devTools
);

export default store;