import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addToStore } from '../store/actions';
const Home = () => {

const dispatch = useDispatch();
const dateNow = Date.now();

const [values, setValues] = useState({
    selectedType: '',
    selectedCategory: '',
    inputValue: '',
})

const [sendFormSucess, setSendFormSucess] = useState(false);


const handleChangeSelectOption = (e) => {
        setValues(prev => ({ ...prev, selectedType: e.target.value}))
}

const handleChangeSelectCategory = (e) => {
    setValues(prev => ({...prev, selectedCategory: e.target.value}));
}

const handleChangeInput = (e) => {
    setValues(prev => ({...prev, inputValue: e.target.value}));
}



const sendForm = (event) => {
    event.preventDefault();
    const formData = {
        date: dateNow,
        type: values.selectedType,
        category: values.selectedCategory,
        value: +values.inputValue,
    }
    dispatch(addToStore(formData));
    setValues({ 
        selectedType: '',
        selectedCategory: '',
        inputValue: ''
    });
    setSendFormSucess(true);
    setTimeout(() => {
        setSendFormSucess(false);
    }, 2000)
}




    return (
        <>  
        <form>
            <label> <input value={values.inputValue} onChange={(e) => {handleChangeInput(e)}} type="number"/> </label>
                <select  value={values.selectedType} onChange={(e) => {handleChangeSelectOption(e)}}>
                    <option value="" disabled>Select type</option>
                    <option value="income">Income</option>
                    <option value="expense">Expense</option>
                </select>


                {values.selectedType && (
                <select  value={values.selectedCategory} onChange={(e) => {handleChangeSelectCategory(e)}}> 
                    {values.selectedType==="income" ? (
                        <>
                            <option value="" disabled>Select category</option>
                            <option value="salary">salary</option>
                            <option value="bonus">bonus</option>
                        </>
                    ): (
                        <>
                            <option value="" disabled>Select category</option>
                            <option value="product">product</option>
                            <option value="taxi">taxi</option>
                            <option value="leisure">leisure</option>
                            <option value="club">club</option>
                            <option value="travel">travel</option>
                        </>
                    )}
                </select>
                )}
            
         <button type="submit" onClick={sendForm} disabled={values.inputValue === '' || values.selectedType === '' || values.selectedCategory === ''}> ADD </button>
        </form> 
        {sendFormSucess && (<h1>Transactions success</h1>)}
        </>
    )
}

export default Home;