import React from "react";
import {  NavLink } from "react-router-dom";


function Header() {
  return (
    <nav>
        <ul>
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/Overview">Overview</NavLink></li>
            <li><NavLink to="/Transactions">Transactions</NavLink></li>
        </ul>
    </nav>
    );
}

export default Header;
