import React, { useState } from 'react';
import { useSelector } from 'react-redux';

const Transactions = () => {
    const [value, setValue] = useState({
        selectedFilter: ''
    });
    
const handleChangeFilter = (e) => setValue(prev => ({...prev, selectedFilter: e.target.value}));    
const getInfoTransactions = useSelector((state) => state.transactions);

const getTransactions = getInfoTransactions.map((transaction, index) => { 
    const date = new Date(transaction.date)

    return (
        <>
            {(
                <li key={index}>
                    Date: {date.getFullYear()} <br/>
                    Type: {transaction.type} <br/>
                    Category: {transaction.category} <br/>
                    Value: {transaction.value} <br/>
                    <br/>
                </li>
            )}
        </>
    )
})
    return ( 
        <>
            {getInfoTransactions.length === 0 ? <h1>No success transactions</h1> : (
                <>
                    <select value={value.selectedFilter} onChange={(e) => {handleChangeFilter(e)}}>
                        <option value="" disabled>Select filter</option>
                        <option value="date">date transaction</option>
                    </select>
                    <ul>
                        {getTransactions}
                    </ul>
                </>
            )}
        </>
   )
}

export default Transactions;