import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../pages/Home';
import Overview from '../pages/Overview';
import Transactions from '../pages/Transactions';

const Routes = () => {

    return (
    <>
        <Switch>
            <Route exact path='/'>
                <Home />
            </Route>
            <Route exact path='/Overview'>
                <Overview />
            </Route>
            <Route exact path='/Transactions'>
                <Transactions />
            </Route>
        </Switch>
    </>
    )
}

export default Routes;