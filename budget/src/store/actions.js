import * as type from './types';

export const addToStore = (obj) => ({
    type: type.ADD_TRANSACTION,
    payload: obj
})

export const setTransactions = (transactions) => ({
    type: type.SET_TRANSACTIONS,
    payload: transactions
})