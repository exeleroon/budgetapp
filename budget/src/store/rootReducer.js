import * as type from './types';


const initialState = { 
    transactions: [
    ],
}
  


const rootReducer = (state = initialState, action) => {
    switch (action.type) {    
        case type.ADD_TRANSACTION:
            return {...state, transactions: [...state.transactions, action.payload]};
        case type.SET_TRANSACTIONS:
            return {...state, transactions: action.payload}
        default: return state;
    }
};

export default rootReducer;